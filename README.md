## NextJS Landing Page Template

This landing page collects emails and stores them in a Google Spreadsheet.

![preview image](docs/preview.png "Landing page preview")

### Stack

This project uses NextJS 13, the "app" directory and the new Route Handlers.

## Requirements

You will need a Google Developer project with the Google Sheets API enabled.

https://support.google.com/googleapi/answer/6158841?hl=en

You will also need to create a service account with a public/private keypair and grant the account access to the Sheets API.

https://developers.google.com/identity/protocols/oauth2/service-account#creatinganaccount

Please ensure the environment file is correctly populated before building/deploying. See below for further details.

## Development

Local development is straightforward and follows the traditional React/NextJS patterns.

Most operations are available via make:

```
$ make
frontend-build                    build the frontend                                          (frontend.mk)
frontend-container-build          build the frontend container. optional TAG (0.0.1)          (frontend.mk)
frontend-container-run            run the frontend container. optional TAG (0.0.1)            (frontend.mk)
frontend-deps                     install the frontend dependencies                           (frontend.mk)
frontend-start-dev                start the development server                                (frontend.mk)
help                              print this help                                             (Makefile)
k-deploy-app                      deploy the application                                      (kubernetes.mk)
temp-delete                       delete temp directory                                       (Makefile)
```

For easy editing, all variables and configuration options are set via a ``.env`` file (e.g. `.env.local`).
See the dummy variables in the provided [`env.local`](app/frontend/env.local) to get started.

> Note: ensure the file is renamed to start with a `.`

```
# the google sheets id
# note: this is the ending string in a google sheets URL
GOOGLE_SHEET_ID="my-google-sheets-id"

# the sheet range to enter items
GOOGLE_SHEET_RANGE="A1:B1"

# the google client email to access the sheets api
GOOGLE_CLIENT_EMAIL="my-google-sheets-client-email"

# the google private key to access the sheets api 
GOOGLE_PRIVATE_KEY="my-google-sheets-private-key"

# the api path when calling the backend. 
NEXT_PUBLIC_API_PATH="/api"

# the version of your application/deployment
NEXT_PUBLIC_APP_VERSION="Alpha"

# the displayed contact email
NEXT_PUBLIC_CONTACT_EMAIL="info@example.com"

# the google analytics id 
NEXT_PUBLIC_GA_MEASUREMENT_ID="G-my-google-analytics-id"

# the page description
NEXT_PUBLIC_PAGE_DESCRIPTION="Happiness Engineering Landing Page Template"

# the page title 
# displayed under the logo
NEXT_PUBLIC_PAGE_TITLE="Landing Template"

# if you wish to add an auto-incrementing date string after the version
# in the form: (MM.YY)
NEXT_PUBLIC_USE_AUTOMATIC_DATE_INCREMENT="true"

```

You can start the development server by first installing the dependencies and then starting the server process with:

```
$ source settings.dev
$ make frontend-deps
$ make frontend-start-dev
```

### Images

Replace the images in `/app/frontend/public` with your chosen background and logo.

### Build and Deploy

The application is built and packaged into a container using the upstream NextJS base and deployed to a Kubernetes cluster.

To build the container:

```
$ make frontend-container-build
```

> Note: You may pass an optional TAG variable to update the image's version: `TAG=0.0.2 make frontend-container-build`

Once a container artifact is available to the cluster (via a registry or local import), you may deploy the application via:

```
$ make k-deploy-app
```

> Note: Deployments are conducted via Kustomize. To edit the deployment's tag or container name, edit the `kustomization.yaml` in the environment's overlay directory, e.g. [kubernetes/overlays/development/landing/kustomization.yaml](kubernetes/overlays/development/landing/kustomization.yaml)

### Exposing services

The deployment includes a service, but does not include any other resources for exposing services outside the cluster.

Currently, for simplicity, we run the application in a private cluster and expose an HTTPS endpoint via [`cloudflared`](https://developers.cloudflare.com/cloudflare-one/tutorials/many-cfd-one-tunnel/).

Traffic flows as follows:

```
    [INTERNET]
        v (https:443)
   [cloudflared]
        v (http:3000)
 [landing-service]
        v (http:3000)
[landing-deployment]
```

Alternatively, for a more robust, "production-ready" solution, you could use an Ingress object via your controller of choice (e.g. [nginx](https://github.com/kubernetes/ingress-nginx)).