KUBERNETES_BASES_DIRECTORY := $(BUILD_DIRECTORY)/kubernetes/bases
KUBERNETES_OVERLAY_DIRECTORY := $(BUILD_DIRECTORY)/kubernetes/overlays

KUBECTL_BIN ?= kubectl
KUBECTL_OPTIONS ?= --dry-run=client
KUBECTL_ACTION ?= apply
KUSTOMIZE_LOAD_RESTRICTOR ?= --load-restrictor LoadRestrictionsNone
KUBECTL_COMMAND ?= | $(KUBECTL_BIN) $(KUBECTL_ACTION) $(KUBECTL_OPTIONS) -f -
KUSTOMIZE_COMMAND ?= $(KUBECTL_BIN) kustomize $(KUSTOMIZE_LOAD_RESTRICTOR)

k-deploy-app: ## deploy the application
k-deploy-app: clean copy-to-build
	$(KUSTOMIZE_COMMAND) \
	  $(KUBERNETES_OVERLAY_DIRECTORY)/$(DEPLOY_ENV)/landing/ \
	  $(KUBECTL_COMMAND)