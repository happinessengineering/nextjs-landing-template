import '@mantine/core/styles.css';
import '@mantine/notifications/styles.css';
import React from 'react';
import { MantineProvider, ColorSchemeScript } from '@mantine/core';
import { Notifications } from '@mantine/notifications';

import "./globals.css"

export const metadata = {
  title: process.env.PAGE_TITLE,
  description: process.env.PAGE_DESCRIPTION,
}

export default function RootLayout(
  { children }
: 
  { children: any }) {
  return (
    <html lang="en">
      <head>
        <ColorSchemeScript />
        <link rel="shortcut icon" href="/favicon.svg" />
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width, user-scalable=no"
        />
      </head>
      <body>
        <MantineProvider>
          <Notifications position='top-right' zIndex={1000}/>
            {children}
        </MantineProvider>
      </body>
    </html>
  );
}
