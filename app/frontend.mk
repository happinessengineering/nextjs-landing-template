frontend-deps: ## install the frontend dependencies
frontend-deps:
	cd app/frontend; \
	  yarn install

frontend-start-dev: ## start the development server
frontend-start-dev:
	cd app/frontend; \
	  yarn dev

frontend-build: ## build the frontend
frontend-build:
	cd app/frontend; \
	  yarn build

frontend-container-build: ## build the frontend container. optional TAG (0.0.1)
frontend-container-build:
	cd app/frontend; \
	  docker build -t $(PROJECT):$${TAG:-0.0.1} -f Dockerfile .

frontend-container-run: ## run the frontend container. optional TAG (0.0.1)
frontend-container-run:
	docker run --rm \
	  -p 3000:3000 \
	  -v $(PWD)/app/frontend/.env.local:/app/.env.local \
	  $(PROJECT):$${TAG:-0.0.1}